+++
title = "Habeltop RPG System"
description = "Hazel designs a little tabletop RPG system for hopefully finally running a campaign with friends."
date = 2021-03-26
draft = false
[extra]
toc = true
+++

Hazel designs a little tabletop RPG system for hopefully finally running a campaign with friends.

This is roughly inspired by a couple versions of the GLOG,
but somewhat simplified and streamlined. A major difference is that the class system
is completely absent, replaced by "roles" which mechanically only manifest
as a starting kit for the character.

Here is the [Blank Character Sheet](https://docs.google.com/document/d/1abX4Lz_LPrIA0YloGtYLRHElYBdmCTXh2L1x32nEBr4/edit?usp=sharing)
as well as an [Example Character](https://docs.google.com/document/d/1nclIkKVxE3cegKzWPlsu-1_KR7Z8FXc1f0w17RLR2zI/edit?usp=sharing).

# What Makes Up a Character
Very first I will go over everything on that makes up a character then I will go more in-depth with each part.

**Name:**  
The in-world name of the character.  
I hope that no further explanation is needed here.

**Role:**  
The architype/role the character takes on.
This just determines starting skills and equipment.

**Level:**  
The current level of the character. Leveling up increases stats.

**Money:**  
The ammount of currency a character has on them.
This could manifest as anything from a sack of gold coins to a bank account depending on the setting.

**Health:**  
The character's ability to avoid major harm. This is elaborated on later.

**Armor:**  
The character's ability to deflect damage. This comes solely from equipment.

**Inventory:**  
The character's belongings and other things they are currently carrying.

**Stats:**  
The column in the top right of the sheet. The character's main stats, as well as the modifier from each stat.

**Injuries:**  
The major injuries and afflictions a character has along with a space to mark what stat they affect.

**Skills:**  
The things the character has training for or otherwise has skill in.

**Other:**  
Other details or notable things about the character.

## Roles
During character creation, a role is chosen. A character's role is the part they play in the party.

The role will list the equipment a character starts with as well as a list of skills to choose `3` from.

## Stat Modifiers
Each of the six base stats also has a "modifier" based on its value.
The modifier is calculated like so: `Stat / 3 - 3`, rounding down to whole numbers.

Modifiers are rarely used outside of the strength modifier.

For convenience, here is a table of the modifiers for each stat value:

{% table() %}
| Stat  | Mod. | Stat     | Mod. | Stat     | Mod. |
|-------|:----:|----------|:----:|----------|:----:|
| 1,2   | -3   | 9,10,11  | 0    | 18,19,20 | +3   |
| 3,4,5 | -2   | 12,13,14 | +1   | 21,22,23 | +4   |
| 6,7,8 | -1   | 15,16,17 | +2   | 24+      | +5   |
{% end %}

## Base Stats
Listed here are the six major character stats and what they affect along with a few
examples of what sort of actions they might be rolled against to determine the success of.

To determine a character's starting stats, roll `4d4` six times. Assign the six results to
whichever stats you wish.

**Strength [STR]:**  
A character's physical brute force and melee ability.  
A strength check is made when attempting a melee attack.  
The strength modifier is applied to the damage they with melee attacks.  
The number of items they can carry on them is equal to their strength.

Examples:
- Pushing, pulling, or lifting something heavy
- Breaking down a door
- Hanging onto a ledge
- Knocking something over

**Dexterity [DEX]:**  
A character's general physical skill, agility, and ranged ability.  
A dexterity check is made when attempting a ranged attack.  
A character's ability to dodge in combat is determined by their dexterity.  

Examples:
- Doing a delicate task, such as soldering an electronic
- Performing and acrobatic feat
- Grabbing onto something to prevent from falling
- Swiping something without being noticed

**Constitution [CON]:**  
A character's physical resilience and overall health.  
A constitution check is made when resisting physical harm or other effects on the body.  
A character's maximum health is determined by their constitution.

Examples:
- Resisting extreme temperatures
- Recovering from illness or the effects of poison
- Dealing with the effects of starvation or dehydration
- Consuming things that would cause one to vomit

**Intelligence [INT]:**  
A character's general knowledge-base.  

Examples:
- Identifying an object
- Researching an obscure topic
- Decyphering an encrypted message
- Utilizing complicated or strange technology

**Wisdom [WIS]:**  
A character's perception and mental fortitude.  
Wisdom checks are used to determine the order of combat.  

Examples:
- Seeing through deceit
- Spotting hidden dangers
- Resisting torture
- Estimating the severity of a threat

**Charisma [CHA]:**  
A character's social skill and charm.  

Examples:
- Claiming to be a different person
- Persuading someone to your side
- Winning the crowd over in an argument
- Leading and organizing a group of people

## Health
Think of health less as how alive a character is, and more as how well they can avoid extreme harm.
Damage could be thought of as the sum of all of the small injuries and fatigue of the character. 

Only when a character's health is fully depleated will they suffer major injury or have the possibility of death.

Health can be restored by rest and medical attention.

A character's maximum health is equal to their constitution plus their current level.

## Skills
Skills represent specific actions or subjects that a character has special
training or intuition for.

A character can automatically succeed at easier
tasks that a skill reasonably applies to.

Tougher actions still require a relevant skill check, but each applicable skill
nets a `+1` modifier to the roll.

## Inventory
All of the items that a character has in their posession are listed here.

A character can hold up to a number of items equal to their strength before they are encumbered.
Small items that can easily fit in a pocket are not counted towards encumberance.

When is encumbered, count their strength and dexterity values as half of what they are.

## Levelling Up
Characters level up after getting through a particularly tough or otherwise important experience
or whenever the GM feels it would be a good time to.

Follow these steps when levelling up a character:

1. For each stat, roll `1d20`. If the roll is higher than the stat's current value, increase the stat by one.
2. Update health to reflect new level and constitution.

# Gameplay
For the most part, gameplay consists of loose roleplay. The GM will present the
world, the current situation, etc. to the players, then they will respond to the
situation, interact with each other, etc. in-character.

## Skill Checks
Whenever a character attempts something that they are not certain to succeed at,
a check against the most relevant stat must be made.

Roll `1d20`, add the relevant stat as well as `+2` for every skill that could be applied,
and if the result is at least `20`, the character succeeds.

If the dice rolls exactly `1` it is a critical failure. The character automatically fails
and could suffer further consequence, should they make sense situationally.

If the dice rolls exactly `20` it is a critical success. The character automatically succeeds
and may succeed beyond their aim, should it make sense situationally.

## Opposed Checks {#opposed-checks}
When a character attempts something that another character is resisting, add `opponent's stat - 10`
to the target number of `20`.

Example:  
A character is trying to knock down another character with `14` strength.
Their roll must total at least `24` (`20 + (14 - 10)`).

Rather than all characters making skill checks, only player characters and NPCs
on their side do so. For example, rather than an NPC making a dexterity check in
attempt to trip a player character, the player themselves makes a dexterity check
in opposition to avoid falling.

## Advantage and Disadvantage
Some circumstances put a character at either an advantage or disadvantage.
For example, having fallen to the ground would put a character at a disadvantage when it comes to defending themselves,
or being in a stance ready to defend themselves would put them at an advantage.

When a character has advantage on a skill check, add a bonus of `+4` to their roll.
When a character has disadvantage, subtract a penalty of `-4`.

# Combat
Combat is initiated when there is to be physical conflict between opposing parties.
Combat takes place in rounds that are equal to roughly 10 seconds of time.

## Turn Order

When combat is initiated, every player character in the party makes a wisdom skill check.
If they succeed, they act before the opposing NPCs each round.
If they fail, they act after the opposing NPCs.
The party decides amongst themselves who acts in what order.

So the resulting turn order is as such:
1. PCs who succeeded wisdom check
2. Opposing NPCs
3. PCs who failed wisdom check

## Each Turn

A character may move up to `DEX * 2` feet and perform one of the following actions.
The may also move twice that distance, but without performing an action.

- Make an attack
- Take a defense stance
- Assist another character in their action
- Attempt to revive a downed character
- Attempt to perform some other arbitrary action

## Attacking
A player character may attack another character by making a skill check
opposed by the target's dexterity.
The stat used depends on the weapon the character is attacking with. 

Melee attacks use the strength stat and may be made at a range of `5 feet`. 

Ranged attacks use the dexterity stat with a penalty of `-2` for
every `10 feet` past the weapon's listed range.  

If the skill check succeeds, roll for damage according to the weapon used.
Individual weapons can have different stats, but here is a rough guideline:

{% table() %}
| Weapon Type              | Damage      |
|--------------------------|:-----------:|
| Unarmed                  | `1d4 + STR` |
| Small Melee Weapons      | `1d6`       |
| Medium Melee Weapons     | `1d6 + STR` |
| Large Melee Weapons      | `1d8 + STR` |
|                          |             |
| Thrown Weapons           | `1d4`       |
| Basic Ranged Weapons     | `1d6`       |
| Heavy Ranged Weapons     | `1d10`      |
| Rapidfire Ranged Weapons | `3d6`       |
{% end %}

Ranged weapons have a reload rate associated ranging between `0` and `12`.
Whenever an attack is made with a ranged weapon, whether the attack hits or not, roll `1d12`.
If the roll is less than or equal to the weapon's reload rate, the weapon is now out of ammo
and a combat action must be spent to reload it, consuming a pack of the requisite ammo.

## Defending
When a player character is attacked, they may choose to either:
- Attempt a dexterity check in order to dodge
- Deliberately take the hit and retaliate with an attack

If a character is currently in a defense stance, they get advantage on their
attempt to dodge or retaliate and are taken out of their defense stance until they
take a turn to re-enter it.

## Downed Characters
Characters are not considered to have sustained major injury for as long as their health is above `0`.
When a character's health reaches `0`, they are considered downed.
A downed character may not act in combat.

A character's health does not go below `0`.
When a character takes damage that *would* bring their health below `0`, they must make a constitution check.

- If they succeed, the character recieves a scar or other permanent marking.
- If they fail, the character recieves a major injury.
- If they critically fail, they die.

## Major Injuries
A major injury is something more serious than the scratches and bruises that will heal easily.
They are more permanent things that require proper medical attention in order to be treated.

Each major injury applies a penalty of `-2` to a specific stat, depending on the type of injury.

Injuries can only be removed by proper medical attention. An untrained person failing to properly
treat an injury may just make things worse.
